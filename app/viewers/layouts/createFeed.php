<div class="col-lg-8 col-lg-offset-2">
	<h3>Editor de noticias</h3>
	<form>
		<label for="title">Titulo</label>
		<input id="title" name="title" class="form-control" placeholder="Titulo de la noticia"/>
		<label for="bodyFeed">Cuerpo</label>
		<input id="bodyFeed" name="body" class="form-control" placeholder="Descripción de la noticia"/>
		<label for="image">Imagen</label>
		<input id="image" name="image" class="form-control" placeholder="Imagen *url completa de la imagen"/>
		<label for="source">Source</label>
		<input id="source" name="source" class="form-control" placeholder="Source"/>
		<label for="publisher">Fecha</label>
		<input id="publisher" name="publisher" class="form-control" placeholder="Fecha de la publicación"/>
	</form>
	<button id="crearFeedController">Crear noticia</button>
	<button id="volver">Volver</button>
</div>