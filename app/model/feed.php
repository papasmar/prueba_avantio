<?php

class Feed{
	
	private $idFeed;
	private $title;
	private $body;
	private $image;
	private $source;
	private $publisher;

	public function __construct(){

	}

	public function getIdFeed(){
		return $this->idFeed;
	}
	public function getTitle(){
		return $this->title;
	}
	public function getBody(){
		return $this->body;
	}
	public function getImage(){
		return $this->image;
	}
	public function getSource(){
		return $this->source;
	}
	public function getPublisher(){
		return $this->publisher;
	}
	public function setTitle($title){
		$this->title = $title;
	}
	public function setBody($body){
		$this->body = $body;
	}
	public function setImage($image){
		$this->image = $image;
	}
	public function setSource($source){
		$this->source = $source;
	}
	public function setPublisher($publisher){
		$this->publisher = $publisher;
	}
	public function obtainFeed($idFeed){
		$feeds = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",null,true);
		$new = [];
		if($feeds){
			$i = 0;
			foreach($feeds as $feed){
				if($feed['idFeed'] == $idFeed){
					$new['idFeed'] = strip_tags($feed['idFeed']);
					$new['title'] = strip_tags($feed['title']);
					$new['body'] = strip_tags($feed['body']);
					$new['image'] = strip_tags($feed['image']);
					$new['source'] = strip_tags($feed['source']);
					$new['publisher'] = strip_tags($feed['publisher']);
				}
				$i++;
			}
		}
		return $new;
	}
	public function obtainFeeds(){

		$feeds = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",null,true);
		$news = [];
		if($feeds){
			$i = 0;
			foreach($feeds as $feed){
				$news[$i]['idFeed'] = $feed['idFeed'];
				$news[$i]['title'] = $feed['title'];
				$news[$i]['body'] = $feed['body'];
				$news[$i]['image'] = $feed['image'];
				$news[$i]['source'] = $feed['source'];
				$news[$i]['publisher'] = $feed['publisher'];
				$i++;
			}
		}
		return $news;
	}
	public function insertFeed(){
		
		
		$oldFeed = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",null,true);
		if(!$oldFeed){
			$idFeed = 0;
		}else{
			foreach($oldFeed as $id){
				$idFeed = $id['idFeed'];
			}
		}
		$idFeed = $idFeed + 1;
		$feed = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",0,true);
		$newFeed = $feed->addChild('feed');
		$newFeed->addAttribute('idFeed',$idFeed);
		$newFeed->addAttribute('title', $this->title);
		$newFeed->addAttribute('body',$this->body);
		$newFeed->addAttribute('image',$this->image);
		$newFeed->addAttribute('source', $this->source);
		$newFeed->addAttribute('publisher',$this->publisher);
		$feed->asXML($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");


		
	    


 	}
 	public function deleteFeed($idFeed){
 		$doc = new DOMDocument();
 		$doc->load($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");
 		$document = $doc->documentElement;
 		$news = $document->getElementsByTagName('feed');
 		foreach($news as $new){
 			$feed = $new->getAttribute('idFeed');
 			if($feed == $idFeed){
 				$document->removeChild($new);
 			}
 			
 		}
 		$doc->save($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");


 		/*$feeds = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",null,true);
		if($feeds){
			foreach($feeds as $feed){
				if($feed['idFeed'] == $idFeed){
					$idFeedBorrar = $feed['idFeed'];
				}
			}
			echo $idFeedBorrar;
			//$feeds->feed[$idFeedBorrar] = null;
			$feeds->asXML($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");
		}*/
 	}
 	public function editFeed($idFeed){
		$feeds = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml",null,true);
		if($feeds){
			foreach($feeds as $feed){
				if($feed['idFeed'] == $idFeed){
					$feed['title'] = $this->title;
					$feed['body'] = $this->body;
					$feed['image'] = $this->image;
					$feed['source'] = $this->source;
					$feed['publisher'] = $this->publisher;
				}
			}
			$feeds->asXML($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");
		}
 	}



	

}

?>