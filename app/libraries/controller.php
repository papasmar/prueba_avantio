<?php 

	//controlador principal
	//se encargara de cargar los modelos y las vistas
	
	class Controller{
		//cargar model
		public function model($modelo){
			require_once $_SERVER['DOCUMENT_ROOT'].'/app/model/' .$modelo .'.php';
			return new $modelo();
		}
		public function vista($vista, $datos = []){
			if(file_exists('/app/viewers/' . $vista . '.php')){
				require_once '/app/viewers/' . $vista . '.php';
			}else{
				die('404');
			}
		}

		
	}

?>