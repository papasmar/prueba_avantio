
$(document).on('click', '#agregar-noticia', vistaNoticias);
$(document).on('click', '.editar-noticia', vistaEditarNoticia);
$(document).on('click', '#editarFeedController', editarNoticia);
$(document).on('click', '#borrarFeedController', borrarNoticia);
$(document).on('click', '#crearFeedController', crearNoticia);
$(document).on('click', '#volver', volver);



function volver(){
  viewerController('listFeeds');
}
function vistaEditarNoticia(){
	var id;
	id = $(this).data('id');
	
  $.ajax({
  	type:'POST',
  	url: "app/viewers/layouts/editFeeds.php?id="+id,
  	success: function(data){
  		$('#cuerpo').html(data);
  	}
  })
      
}

function editarNoticia(){

 id = $(this).data('id');
 $.ajax({
        type: "POST",
        url:  "app/controllers/docAjax.php",
        data: { 'action' : 'actualizar', 'id':id, 'form':$('form').serialize()},
        success: function (data) {
         viewerController('listFeeds');
       }
    })        
}
function crearNoticia(){
  $.ajax({
        type: "POST",
        url:  "app/controllers/docAjax.php",
        data: { 'action' : 'crear', 'form':$('form').serialize()},
        success: function (data) {
         viewerController('listFeeds');
       }
    })
}

function borrarNoticia(){
  id = $(this).data('id');
 $.ajax({
        type: "POST",
        url:  "app/controllers/docAjax.php",
        data: { 'action' : 'borrar', 'id':id},
        success: function (data) {
          viewerController('listFeeds');
        }
    })        
}

function vistaNoticias(){
  viewerController('createFeed');
}

function viewerController(viewer){

$.ajax({
    type: "POST",
    url: "app/viewers/layouts/"+viewer+".php",
    success: function (data) {
      $('#cuerpo').html(data);
    }
  })
}
