<?php
$xml = new DomDocument('1.0', 'UTF-8');
$raiz = $xml->createElement('feeds');
$raiz = $xml->appendChild($raiz);
$xml->save($_SERVER['DOCUMENT_ROOT']."/app/config/feed.xml");
include $_SERVER['DOCUMENT_ROOT']."/app/controllers/controllerFeed.php";

$urlPais = 'https://elpais.com/rss/elpais/portada_completo.xml';
$urlMundo = 'https://e00-elmundo.uecdn.es/rss/portada.xml';

$max_posts=5;

$posts1 = getPostTodayPais($urlPais, $max_posts);
$posts2 = getPostTodayMundo($urlMundo, $max_posts);


foreach($posts1 as $post1){
	$controller = (new ControllerFeeds())->insertFeedModel($post1);
}
foreach($posts2 as $post2){
	$controller = (new ControllerFeeds())->insertFeedModel($post2);
}

function getPostTodayPais($url, $max_posts){
	$feed = [];
	$num_post=1;
	$today = date('Y-m-d');
	$posts = simplexml_load_string(file_get_contents($url));

	foreach($posts->channel->item as $post){ 
		if(postToDay($post->pubDate, $today)){
			
			$feed[] = [
				'title' => strip_tags($post->title),
           	 	'body' => strip_tags($post->description),
           		'image' => strip_tags($post->enclosure['url']),
           		'source' => 'El Pais',
           		'publisher' => date('d/m/Y', strtotime(strip_tags($post->pubDate)))
			];
			$num_post++;

		}
		if($num_post > $max_posts)
			break;   
	}
	return $feed;
}
function getPostTodayMundo($url, $max_posts){

	$num_post=1;
	$today = date('Y-m-d');
	$posts = simplexml_load_string(file_get_contents($url));
	$feed = [];
	foreach($posts->channel->item as $post){ 
		if(postToDay($post->pubDate, $today)){
			$feed[] = [
				'title' => strip_tags($post->title),
           	 	'body' => strip_tags($post->children('media', true)->description),
           		'image' => strip_tags($post->children('media', true)->content->attributes()),
           		'source' => 'El mundo',
           		'publisher' => date('d/m/Y', strtotime(strip_tags($post->pubDate)))

			];
			$num_post++;
		}
		if($num_post > $max_posts)
			break;   
	}
	return $feed;
}


function postToDay($date, $today){
	//Format date
	$timestamp = strtotime($date);
	$dateFormat = date('Y-m-d', $timestamp);
	
	if(strtotime($dateFormat) == strtotime($today))
		return true;
	else
		return false;
}
?>