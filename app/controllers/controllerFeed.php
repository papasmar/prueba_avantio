<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/app/libraries/controller.php');

class controllerFeeds extends controller{
	
	private $feedModel;
	
	public function __construct(){
		$this->feedModel = $this->model('feed');
	}
	public function insertFeedModel($aFeed){
		if(empty($aFeed['image'])){
			$aFeed['image'] = '/app/viewers/image/no-image.png';
		}
		$this->feedModel->setTitle($aFeed['title']);
		$this->feedModel->setBody($aFeed['body']);
		$this->feedModel->setImage($aFeed['image']);
		$this->feedModel->setSource($aFeed['source']);
		$this->feedModel->setPublisher($aFeed['publisher']);
		$this->feedModel->insertFeed();
	}
	public function editFeed($aFeed){


		$this->feedModel->setTitle($aFeed['title']);
		$this->feedModel->setBody($aFeed['body']);
		$this->feedModel->setImage($aFeed['image']);
		$this->feedModel->setSource($aFeed['source']);
		$this->feedModel->setPublisher($aFeed['publisher']);
		$this->feedModel->editFeed($aFeed['idFeed']);
	}
	public function obtainFeedsModel(){
		$res = $this->feedModel->obtainFeeds();
		return $res;
	}
	public function deleteFeed($idFeed){
		$this->feedModel->deleteFeed($idFeed);
	}
	public function obtainFeedModel($idFeed){
		return $this->feedModel->obtainFeed($idFeed);
	}

}

?>