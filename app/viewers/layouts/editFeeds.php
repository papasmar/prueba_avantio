<?php 
$idFeed = $_REQUEST['id'];
include($_SERVER['DOCUMENT_ROOT'].'/app/controllers/controllerFeed.php');
$controller = new ControllerFeeds();

$feed = $controller->obtainFeedModel($idFeed);

echo '
<div class="col-lg-8 col-lg-offset-2">
<h3>Editor de noticias</h3>
<form>
	<label for="title">Titulo</label>
	<input id="title" name="title" class="form-control" value="'.$feed['title'].'"/>
	<label for="bodyFeed">Cuerpo</label>
	<input id="bodyFeed" name="body" class="form-control" value="'.$feed['body'].'"/>
	<label for="image">Imagen</label>
	<input id="image" name="image" class="form-control" value="'.$feed['image'].'"/>
	<label for="source">Source</label>
	<input id="source" name="source" class="form-control" value="'.$feed['source'].'"/>
	<label for="publisher">Fecha</label>
	<input id="publisher" name="publisher" class="form-control" value="'.$feed['publisher'].'"/>
</form>
<button id="editarFeedController" data-id="'.$feed['idFeed'].'">Editar</button><button id="borrarFeedController" data-id="'.$feed['idFeed'].'">Borrar</button>
<button id="volver">Volver</button>
</div>';
